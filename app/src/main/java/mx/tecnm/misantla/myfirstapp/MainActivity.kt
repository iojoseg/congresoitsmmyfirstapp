package mx.tecnm.misantla.myfirstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

     btn_confirmar.setOnClickListener {

         var nombre = edt_nombre.text.toString()
         var apellido = edt_apellido.text.toString()


         txt_result.setText( nombre +" "+ apellido)

         Toast.makeText(this, "Funciono ok",Toast.LENGTH_LONG).show()

         edt_nombre.text.clear()
         edt_apellido.text.clear()
     }

    }
}